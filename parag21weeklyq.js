(function(){
    //答案庫在此
    var answerDic = {};
    answerDic["在救麗莎的雪原打倒吉爾迦斯後出現的先知是誰"] = "斐埃爾";
    answerDic["聖光之錐使吉爾迦斯跪下後，需要接甚麼技能"] = "審判之刃";
    answerDic["第一次遇見艾爾班騎士團成員是在哪個城鎮"] = "塔拉";
    
    answerDic["第一次遇見艾薇琳的地方是"] = "希德斯特雪原";
    answerDic["審判之刃的副技能哪一個需要做使者武器3次"] = "利刃洗禮";
    answerDic["艾爾班騎士團訓練所最高中級獨自一人完成會獲得"] = "更上一層的";
    
    answerDic["艾爾班騎士團所侍奉的神祇是"] = "主神艾托恩";
    answerDic["亞特使用的武器為何"] = "神聖雙手劍";
    answerDic["獨自前往希德斯特雪原並被米列希安及騎士團員拯救的角色是"] = "麗莎";
    
    answerDic["若看見金色的星星升向空中代表發生了什麼事"] = "有人成為了使徒";
    answerDic["在塔拉王城戰中保護的人是誰"] = "艾蕾蒙";
    answerDic["神聖騎士團每週任務，獲得戰利品吧是"] = "使者之環";

    answerDic["最先學習到的騎士團技能是"] = "聖盾庇護";
    answerDic["艾爾班騎士團巨魔像的頭盔毛色為藍色時，需要哪種攻擊才造成傷害"] = "魔法";
    answerDic["G19拉赫王城最終決戰一次對付幾隻使徒"] = "2隻";
    
    answerDic["第一次在塔拉王城門口遇到的騎士團成員為誰"] = "亞特";
    answerDic["在與艾薇琳一同協力打吉爾迦斯時出來幫忙的角色為誰"] = "托爾維斯";
    answerDic["第一次遇見的艾爾班騎士團成員是"] = "亞特";

    //2nd week below
    answerDic["首次出現在G20的使徒名稱是"] = "傑巴赫";
    answerDic["以下哪一位不是先知"] = "彭華幹";
    answerDic["春分組組長是誰"] = "畢奈";
    
    answerDic["夏至組組長是誰"] = "卡茲遠";
    answerDic["請問托爾維斯是以下哪一組的組長"] = "秋收組";
    answerDic["可開啟阿瓦隆門的鑰匙名稱是"] = "啟示之證";
    
    answerDic["以下哪一個城鎮有艾爾班騎士團的秘密集合點"] = "艾明瑪夏";
    answerDic["使用聖靈同步與寵物連結瞬間會有"] = "短暫無敵時間";
    answerDic["為什麼艾爾班騎士團於歷史上銷聲匿跡"] = "為了暗中保護世界不被異界之神入侵";
    
    answerDic["卡茲遠懷疑騎士團的誰是先知的內應"] = "托爾維斯";
    answerDic["G20新增的神聖技能為"] = "聖靈同步";
    answerDic["復活的權杖中寵物最多可以復活幾次"] = "3次";
    
    answerDic["為什麼卡茲遠未前往艾明馬夏與其他人會合"] = "想和米列希安單獨見面";
    answerDic["以下哪一個是騎士團員艾薇琳所持的武器"] = "神聖小型騎士槍";
    answerDic["優雅的春天女王是誰"] = "畢奈";
    
    answerDic["在斯卡哈海岸木橋上與托爾維斯對打的是誰"] = "卡茲遠";
    answerDic["在凱安港口海邊下那一項不是指定料理"] = "高傲牛排";
    answerDic["見習騎士團系統通常被米列希安稱為"] = "問題兒童騎士團";
    
    //3rd week below
    answerDic["前往神都需要消耗什麼道具"] = "法利亞斯的碎片";
    answerDic["武鬥家中跟昇龍拳(上勾拳)很像的技能是"] = "連續技能:螺旋勾拳";
    answerDic["G2聖騎士神聖之心Rank1為哪種騎士"] = "光之騎士";
    
    answerDic["新創角色若不進行重生並將等級提升至100可獲得什麼稱號"] = "徘徊地上的星星";
    answerDic["每天都在割羊毛撿蜘蛛絲的地獄生活是"] = "紡織地獄";
    answerDic["當怪物防禦的時候使用哪種技能較好"] = "重擊";
    
    answerDic["煉金術合成技能需要使用"] = "乾式火盆";
    answerDic["在提爾克那村莊教堂內服務的神父名字是"] = "米恩";
    answerDic["希德斯特雪原的熊是由誰變成的"] = "特拉克";
    
    answerDic["巨龍騎士的號角是召換"] = "亞多利爾";
    answerDic["旋風擺蓮腿通常簡稱為"] = "風車";
    answerDic["半神化除了茉麗安之力還可以選擇"] = "妮潘之力";
    
    answerDic["貿易的貨幣叫做"] = "杜卡特";
    answerDic["以下哪個角色不是武鬥家任務主要的角色"] = "克莉絲";
    answerDic["以下哪一個不是攻略夢幻萊比地下城可獲得的稱號"] = "撲倒蘿莉魅魔的";
    
    answerDic["在附近使用弓箭類武器，可以在弓箭上點火的技能為"] = "營火";
    answerDic["三勇士以下哪個角色不是"] = "克莉絲";
    answerDic["西德斯特雪原的熊喜歡吃甚麼"] = "魔力藥草";

    answerDic["瑪麗的父親是誰"] = "瑪洛士";
    answerDic["巴雷斯村長肩上的老鷹叫什麼名字"] = "許納貝爾";
    
    //4th week below
    answerDic["在伊利亞G18劇情中是誰魔化了"] = "魯艾利";
    answerDic["魯艾利最後成功進行了什麼儀式"] = "召喚異神至愛爾琳";
    answerDic["G18凱薩首領拿的使用的武器技能是"] = "雙槍";
    
    answerDic["寶藏獵人是哪一族的後裔"] = "班族 ";
    answerDic["下面哪一位被稱作「伊利尼德」並擊敗了龍族讓伊利亞大陸恢復生機"] = "妮潘";
    answerDic["變身為各式怪物角色的武器道具是"] = "捕夢者";
    
    answerDic["英雄才能最美英雄DIVA的才能是"] = "音樂";
    answerDic["魔法製造幸運的兔子腳材料不需要什麼"] = "綠寶石核心";
    answerDic["是誰照顧幼年的特拉克"] = "貝瑞克希德";
    
    answerDic["忍者技能中集氣越久範圍跟殺傷力越大是"] = "櫻花雨";
    answerDic["下列哪個不是英雄"] = "蜜莉亞";
    answerDic["魯艾利和特拉克犧牲蜜莉亞是為了召喚誰"] = "馬夏";
    
    answerDic["夏瑪拉變身後變為哪一個"] = "黑豹";
    answerDic["戴著老鷹面具的薩滿是誰"] = "阿庫拉 ";
    answerDic["將四周圍所有的怪物以絲線綁住拉近自身為"] = "誘惑的圈套";
    
    let checkQuestion=function checkQuestion(){
        // Try to get question from <div class="question"> <div class="topic">
        var isQAGotFromSimilarityMatch = false;
        
        var topicDiv = document.querySelector('.question .topic');
        var answerInLi = document.querySelectorAll('.question > ol > li');
        
        var currQues = null;
        var currAns = null;
        
        if (topicDiv) {
            console.log("問題為 \'" + topicDiv.innerHTML + "\'");
            currQues = topicDiv.innerHTML;
            
            if (currQues  && currQues == "") {
                return;
            }
            
            //remove punctuation marks and spaces
            currQues = currQues.replace("？","");
            currQues = currQues.replace("?","");
            currQues = currQues.replace("，","");
            currQues = currQues.replace("！","");
            currQues = currQues.replace("!","");
            currQues = currQues.replace("。","");
            currQues = currQues.replace(".","");
            currQues = currQues.replace(" ","");
            
            if (!(currQues in answerDic)) {
                console.log("問題為 \'" + currQues + "\' 但是答題庫沒有這個問題 (可能某些字串不符), 搜索最接近此問題的key...");
                var closestQuestionMatched = findClosestQuestionMatch(currQues);
                if (closestQuestionMatched != null) {
                    console.log("在答題庫搜到最接近此問題的問題為 \'" + closestQuestionMatched + "\'");
                    currQues = closestQuestionMatched;
                    isQAGotFromSimilarityMatch = true;
                }
                else {
                    console.log("無法在答題庫搜到任何與此問題相似的問題.");
                    currQues = null;
                    return;
                }
            }
        }
        
        if (answerInLi && answerInLi.length > 0) {
            var foundAnswers = [];
            
            for(var i = 0; i < answerInLi.length; i++) {
                var regexAns = answerInLi[i].innerHTML.trim().match(/"([^']+)"/);
                var extractedAnswer = (regexAns && regexAns[1]) ? regexAns[1] : answerInLi[i].innerHTML.trim();
                
                console.log("答案 #" + i + ": \'" + extractedAnswer + "\'");
                foundAnswers.push(extractedAnswer);
            }
            
            // try to get answer from answerDic
            var answerToUse = answerDic[currQues];
            var answerIdx = foundAnswers.indexOf(answerToUse);
            if (answerIdx <= -1) {
                console.log("網頁答案列表中找不到該問題的答案 '" + answerToUse + "', 尋找最接近的答案...");
                var closestAnswerMatched = findClosestStringMatched(answerToUse, foundAnswers);
                if (closestAnswerMatched != null) {
                    console.log("最接近答題庫答案的答案為 \'" + closestAnswerMatched + "\'");
                    currAns = closestAnswerMatched;
                    isQAGotFromSimilarityMatch = true;
                }
                else {
                    console.log("無法搜到任何相似的答案.");
                    currAns = null;
                    return;
                }
            }
            else
            	currAns = answerToUse;
            
            var answerIdx = foundAnswers.indexOf(currAns);
            for(var i = 0; i < answerInLi.length; i++) {
                if (i == answerIdx) {
                    if (isQAGotFromSimilarityMatch)
                        answerInLi[i].style.color = "orange";
                    else
                        answerInLi[i].style.color = "red";
                }
                else {
                    answerInLi[i].style.color = "white";
                }
            }
        }
    };
    
    // Credit goes to https://stackoverflow.com/a/36566052
    function similarity(s1, s2) {
        var longer = s1;
        var shorter = s2;
        if (s1.length < s2.length) {
            longer = s2;
            shorter = s1;
        }
        var longerLength = longer.length;
        if (longerLength == 0) {
            return 1.0;
        }
        return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength);
    }
    
    // Credit goes to https://stackoverflow.com/a/36566052
    function editDistance(s1, s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();

        var costs = new Array();
        for (var i = 0; i <= s1.length; i++) {
        var lastValue = i;
        for (var j = 0; j <= s2.length; j++) {
            if (i == 0)
                costs[j] = j;
            else {
                if (j > 0) {
                    var newValue = costs[j - 1];
                    if (s1.charAt(i - 1) != s2.charAt(j - 1))
                        newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
                    costs[j - 1] = lastValue;
                    lastValue = newValue;
                }
            }
        }
        if (i > 0)
            costs[s2.length] = lastValue;
        }
        return costs[s2.length];
    }
    
    function findClosestQuestionMatch(foundQuestion) {
        var keyInArray = [];
        for (var k in answerDic) {
            keyInArray.push(k);
        }
        return findClosestStringMatched(foundQuestion, keyInArray);
    }
    
    function findClosestStringMatched(stringToMatch, stringArray) {
        var closestStringMatched = "";
        var highestMatchedScore = 0.0;
        var LOWEST_SCORE_THRESHOLD = 0.4; //anything lower than 0.4 will be ignored directly
        for (var i = 0; i < stringArray.length; i++) {
            var currScore = similarity(stringToMatch, stringArray[i]);
            if (currScore <= LOWEST_SCORE_THRESHOLD) 
                continue;
            if (currScore > highestMatchedScore) {
                highestMatchedScore = currScore;
                closestStringMatched = stringArray[i];
            }
        }
        
        if (highestMatchedScore == 0.0)
            return null;
        
        return closestStringMatched;
    }
    
    var timer = setInterval(function(){ checkQuestion() }, 500);
    alert("已載入 (last update: 12 July 2018 1343 - 更新第4週問題答案)");
})();